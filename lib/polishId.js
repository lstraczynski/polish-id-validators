/**
 * polishId.js
 * @description Polish ID number validator
 * @author Łukasz Strączyński (lstraczynski@gmail.com)
 * @date 27.09.2019
 */

function isValidFormat(input) {
    return /^([A-Z]{3}\d{6})$/.test(input);
}

function checksum(input) {
    const offset = 'A'.charCodeAt(0);
    const weights = [7, 3, 1, 9, 7, 3, 1, 7, 3];
    return input.split('').reduce((sum, char, index) => {
        const value = index < 3 ? char.charCodeAt(0) - offset + 10 : Number(char);
        return sum + value * weights[index];
    }, 0) / 10;
}

function validate(input) {
    if (!isValidFormat(input)) {
        return false;
    }
    return Number.isInteger(checksum(input));
}

module.exports = validate;