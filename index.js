const polishId = require('./lib/polishId');
const pesel = require('./lib/pesel');

module.exports = {
    polishId,
    pesel
};
