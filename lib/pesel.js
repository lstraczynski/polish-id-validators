/**
 * pesel.js
 * @description PESEL number validator
 * @author Łukasz Strączyński (lstraczynski@gmail.com)
 * @date 27.09.2019
 */

function validate(input) {
    if (!/^(\d{11})$/.test(input)) {
        return false;
    }

    const weights = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
    const controlNumber = Number(input.slice(-1));
    const preChecksum = input.slice(0, -1).split('').reduce((sum, char, index) => {
        return sum + Number(char) * weights[index];
    }, 0) % 10;

    return ((10 - preChecksum) % 10) === controlNumber; 
}

module.exports = validate;
